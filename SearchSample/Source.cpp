#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>


//■定数宣言
#define MAP_SIZE	23		//マップのサイズ

enum TileStatus	//マスの状態
{
	NONE,	//何もしてない
	OPEN,	//スコア計算済み
	CLOSED	//通過済み
};




//■構造体宣言
struct POINT	//位置情報に使用
{
	int x, y;
};


struct AStarNode	//１マスの情報
{
	TileStatus	status;	//状態（NONE, OPEN, CLOSED)
	int			c;		//実コスト（スタート地点からの距離）
	int			h;		//ヒューリスティック（ゴールまでの直線距離）
	int			s;		//スコア(c+h)
	AStarNode*	parent;	//親の位置（このマスの前に通るマス）
	POINT		pos;	//そのマスの座標
};



//■グローバル変数
int map[MAP_SIZE][MAP_SIZE];	//マップ
AStarNode node[MAP_SIZE][MAP_SIZE];	//探索用配列
POINT playerPos;	//キャラの位置
POINT goalPos;		//ゴールの位置


//■プロトタイプ宣言
void CreateMap();
void InitPosition();
void DrawMap();
void ScoreCalc(int x, int y, int step, AStarNode* parentNode);	//スコア計算
bool Search(int x, int y, int step);	//探索
void InitSearch();	//探索準備（初期化）



////////////////////////////////////////////////////////////////////////////////////
//エントリーポイント
void main()
{
	srand(time(0));

	//迷路作成
	CreateMap();

	//プレイヤーとゴールの位置決定
	InitPosition();

	//繰り返し
	while (true)
	{
		//探索用配列の初期化
		InitSearch();

		//経路探索
		Search(goalPos.x, goalPos.y, 0);

		//今いるマスの親マス

		AStarNode* pParent = node[playerPos.x][playerPos.y].parent;

		//親がいない ＝ ゴールに着いた
		if (node[playerPos.x][playerPos.y].parent == NULL)
		{
			break;
		}

		//親マスの位置に移動
		playerPos = pParent->pos;

		//マップ表示
		DrawMap();

		//一時停止
		std::system("pause");
	}

}
////////////////////////////////////////////////////////////////////////////////////


//自動迷路生成（棒倒し法）
void CreateMap()
{
	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int y = 0; y < MAP_SIZE; y++)
		{
			//外周だったら
			if (x == 0 || x == MAP_SIZE - 1 || y == 0 || y == MAP_SIZE - 1)
			{
				//壁にする
				map[x][y] = 1;
			}

			//それ以外
			else
			{
				//基点となる壁を作成
				if (x % 2 == 0 && y % 2 == 0)
				{
					map[x][y] = 1;

					//４方向のどこかを壁にする
					static const int dx[] = { 1, 0, -1, 0 };
					static const int dy[] = { 0, 1, 0, -1 };
					int r;
					do
					{
						if (y == 2)
							r = rand() % 4;
						else
							r = rand() % 3;
					} while (map[x + dx[r]][y + dy[r]] != 0);

					map[x + dx[r]][y + dy[r]] = 1;
				}
			}
		}
	}
}

//プレイヤーとゴールの位置を決める
void InitPosition()
{
	int x, y;

	//プレイヤーの位置を決める
	do
	{
		//ランダムに位置を決めて
		x = rand() % MAP_SIZE;
		y = rand() % MAP_SIZE;
	} while (map[x][y] == 1);	//そこが壁ならやり直し
	playerPos.x = x;
	playerPos.y = y;


	//ゴールの位置を決める
	do
	{
		x = rand() % MAP_SIZE;
		y = rand() % MAP_SIZE;
	} while (map[x][y] == 1 || (playerPos.x == x && playerPos.y == y));	//そこが壁か、プレイヤーと同じ位置ならやり直し
	goalPos.x = x;
	goalPos.y = y;
}



//マップを表示
void DrawMap()
{
	std::system("cls");	//画面クリア
	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int y = 0; y < MAP_SIZE; y++)
		{
			if (x == playerPos.x && y == playerPos.y)
				printf("◎");
			else if (x == goalPos.x && y == goalPos.y)
				printf("Ｇ");
			else if (map[x][y] == 1)
				printf("■");
			else
				printf("　");
		}
		printf("\n");
	}
}


//探索準備（初期化）
void InitSearch()
{
	//全てのマス
	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int y = 0; y < MAP_SIZE; y++)
		{
			//初期値にする
			node[x][y].status = NONE;
			node[x][y].parent = NULL;
			node[x][y].pos.x = x;
			node[x][y].pos.y = y;
			node[x][y].c = node[x][y].h = node[x][y].s = 0;
		}
	}
}



//探索
//引数：x,y		ゴールの位置
//引数：step	再帰呼び出しの深さ
//戻値：ゴールに到達できるかどうか
bool Search(int x, int y, int step)
{
	//そのマスが現在地だったら終わり
	if (playerPos.x == x && playerPos.y == y)
	{
		return true;
	}

	//周囲４方向
	static const int dx[]{-1, 0, 1, 0};
	static const int dy[]{0, 1, 0, -1};
	for (int i = 0; i < 4; i++)
	{
		//そのマスが通れないなら何もしない
		if (map[x + dx[i]][ y + dy[i]] == 1)
		{
			continue;
		}

		//既に調査済みなら何もしない
		if (node[x + dx[i]][y + dy[i]].status != NONE)
		{
			continue;
		}

		//スコアの計算
		ScoreCalc( x + dx[i], y + dy[i], step, &node[x][y]);
	}

	//基準マスをクローズ
	node[x][y].status = CLOSED;


	//OPENマスで一番スコアが小さいマスを探す
	int minScore = 9999999;	//暫定最小スコア
	POINT nextPos;			//次の基準点
	for (int xx = 0; xx < MAP_SIZE; xx++)
	{
		for (int yy = 0; yy < MAP_SIZE; yy++)
		{
			//OPENマスで暫定最小スコアより小さかったら
			if (node[xx][yy].status == OPEN && node[xx][yy].s < minScore)
			{
				//そこを暫定にする
				minScore = node[xx][yy].s;
				nextPos.x = xx;
				nextPos.y = yy;
			}
		}
	}


	//OPENマスが見つからない ＝ 到達不可能
	if (minScore == 9999999)
	{
		return false;
	}


	//見つけたマスを基準点にして探索継続
	return Search(nextPos.x, nextPos.y, step + 1);
}


//マスのスコア計算
//引数：x,y			調べたいマスの座標
//引数：step		再帰呼び出しの深さ　＝　スタート地点からの距離
//引数：pParentNode	親のマス（ここのマスの前に通るマス）の情報
void ScoreCalc(int x, int y, int step, AStarNode* pParentNode)
{
	//ステータスをOPENに
	node[x][y].status = OPEN;

	//コスト
	node[x][y].c = step;

	//ヒューリスティック
	node[x][y].h = abs(goalPos.x - x) + abs(goalPos.y - y);

	//スコア
	node[x][y].s = node[x][y].c + node[x][y].h;

	//親マス
	node[x][y].parent = pParentNode;
}
